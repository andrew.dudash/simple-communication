;;;; simple-communication.lisp

(in-package #:simple-communication)

(defclass message-reader ()
  ((message-length)
   (message-buffer)
   (read-count :initform 0)
   (read-state :initform 'awaiting-header-prefix)
   (ready-messages :initform (list))))

(defparameter *header-prefix* 1)

(defmethod scan-symbol ((message-reader message-reader) symbol)
  (with-slots (read-state message-length message-buffer read-count ready-messages) message-reader
    (case read-state
      (awaiting-header-prefix
       (when (eq symbol *header-prefix*)
	 (setf read-state 'awaiting-message-length)))
      (awaiting-message-length
       (if (= symbol 0)
	   (progn
	     (setf read-state 'awaiting-header-prefix))
	   (progn
	     (setf read-state 'awaiting-message)
	     (setf message-length symbol)
	     (setf read-count 0)
	     (setf message-buffer (make-array message-length)))))
      (awaiting-message
       (progn
	 (setf (aref message-buffer read-count) symbol)
	 (incf read-count)
	 (when (= read-count message-length)
	   (push (deserialize-message message-buffer) ready-messages)
	   (setf read-state 'awaiting-header-prefix)))))))

(defun deserialize-message (message-bytes)
  (yason:parse (coerce (loop for symbol across (subseq message-bytes 2) collect (code-char symbol)) 'string)))
  
(defmethod pop-message ((message-reader message-reader))
  (with-slots (ready-messages) message-reader
    (pop ready-messages)))

(defun create-message (message-content)
  (concatenate 'vector (vector *header-prefix* (length message-content))
	       message-content))

(defun send-message (client-socket message-content)
  (let ((message (create-message message-content)))
    (write-sequence message (usocket:socket-stream client-socket))))

(defun send-json-message (client-socket data)
  (let ((message (create-json-message data)))
    (write-sequence message (usocket:socket-stream client-socket))))

(defun create-json-message (data)
  (create-message (coerce (loop for symbol across (with-output-to-string (out)
						    (yason:encode data out))
			     collect (char-code symbol)) 'vector)))

#+nil
(let ((client-socket (usocket:socket-connect "localhost" 5053 :element-type '(unsigned-byte 8))))
  (send-message client-socket (create-json-message (list 1 2 3)))
  (usocket:socket-close client-socket))
  

(defun handle-message (client-socket message)
  (declare (ignore client-socket))
  (format t "SERVER RECEIVED: ~a~%" message))	  

(defmethod scan-stream ((message-reader message-reader) stream)
  (let ((message))
    (loop until (setf message (pop-message message-reader))
       do (scan-symbol message-reader (read-byte stream)))
    message))

(defmethod scan-message ((message-reader message-reader) stream)
  (yason:parse
   (coerce (loop for symbol across (scan-stream message-reader stream)
	      collect (code-char symbol))
	   'string)))

(defclass server ()
  ((thread :initform nil)
   (lock :initform (bordeaux-threads:make-lock))
   (address :initarg :address :initform "localhost")
   (port :initarg :port :initform (error "Server needs port argument."))
   (kill-event :initform nil)
   (max-connection-queue :initarg :max-connection-queue :initform 1)
   (message-handler :initarg :message-handler :initform (error "Server needs message-handler."))))

(defun start-handler (client-socket message-handler)
  (bordeaux-threads:make-thread
   (let ((stream-buffer (vector 1024))
	 (message-reader (make-instance 'message-reader)))
     (lambda ()
       (unwind-protect
	    (handler-case
		(loop while (bordeaux-threads:with-lock-held (*server-lock*)
			      (not *server-kill*))
		   do 
		     (loop for index below (read-sequence stream-buffer (usocket:socket-stream client-socket))
			do (scan-symbol message-reader (aref stream-buffer index)))
		     (let ((message))
		       (loop while (setf message (pop-message message-reader))
			  do (funcall message-handler client-socket message))))
	      (end-of-file () nil))
	 (usocket:socket-close client-socket))))
   :name (format nil "CLIENT-THREAD: ~a:~a"
		 (usocket:get-peer-address client-socket)
		 (usocket:get-peer-port client-socket))))

(defmethod start ((server server))
  (with-slots (lock thread address port kill-event max-connection-queue message-handler) server
    (bordeaux-threads:with-lock-held (lock)
      (unless (and (bordeaux-threads:threadp thread) (bordeaux-threads:thread-alive-p thread))
	(setf server nil)
	(setf thread
	      (bordeaux-threads:make-thread
	       (lambda ()
		 (let ((client-threads)
		       (server-socket (usocket:socket-listen address port
							     :reuse-address t
							     :backlog max-connection-queue
							     :element-type '(unsigned-byte 8))))
		   (unwind-protect
			(loop while (bordeaux-threads:with-lock-held (lock)
				      (not kill-event))
			   do (handler-case
				  (push (start-handler (bordeaux-threads:with-timeout (0.5)
							 (usocket:socket-accept server-socket))
						       message-handler)
					client-threads)
				(bordeaux-threads:timeout () nil)
				(usocket:timeout-error () nil)))
		     (usocket:socket-close server-socket))))
	       :name (format nil "SERVER-THREAD: ~a:~a"
			     address
			     port)))))))

(defmethod stop ((server server))
  (with-slots (thread lock kill-event) server
    (when (and (bordeaux-threads:threadp thread) (bordeaux-threads:thread-alive-p thread))
      (bordeaux-threads:with-lock-held (lock)
	(setf kill-event t))
      (bordeaux-threads:join-thread thread)
      (setf thread nil))))

(defmethod destroy ((server server))
  (with-slots (thread lock) server
    (when (bordeaux-threads:threadp thread)
      (bordeaux-threads:destroy-thread thread)
      (setf thread nil))))
