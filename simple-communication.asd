;;;; simple-communication.asd

(asdf:defsystem #:simple-communication
  :description "Describe simple-communication here"
  :author "Andrew \"Drew\" Dudash <andrew.dudash@protonmail.com>"
  :license  "AGPL"
  :version "0.0.1"
  :serial t
  :depends-on (#:uiop #:usocket #:bordeaux-threads #:yason)
  :components ((:file "package")
               (:file "simple-communication")))
